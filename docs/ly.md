[home](https://gitlab.com/JohnCorbs/debian-on-btrfs-on-pc-and-mbp)
## ly
**as user, not root**

```
git clone -b v0.6.0 --recurse-submodules https://github.com/fairyglade/ly
cd ly
make
sudo make install installsystemd
sudo systemctl enable ly.service
```
config in `/etc/ly/config.ini`


