# Debian on btrfs on PC
Starting as a clone of my other repository [here](https://gitlab.com/JohnCorbs/macbookpro-13-3). This repository will show how I myself have gotten debian functional onto my pc like my macbook repo, ~~while also having setup everyting to my preferences :)~~

[TOC]

## prerequisits:

**Have a existing instance of linux with btrfs-progs, debootstrap, and an internet connection**
a link to my guide on creating such usb can be found [here](https://gitlab.com/-/snippets/3603802)

## Install
### make sure wifi is connected:
you know how to do this
### partitions ur disk & mount:
```
≥512M EFI partition:
    ef00 type
    mkfs.fat -F 32 /dev/nvme0n1p1
Partition for btrfs:
    8300 type
    mkfs.btrfs /dev/nvme0n1p2
Partition for swap:
    8200 type
    in gdisk starting sector is -16G
```
```
mount /dev/nvme0n1p2 /mnt
cd /mnt
mkdir debian
btrfs subvolume create debian/@root
btrfs subvolume create debian/@home

btrfs subvolume create debian/@snapshots

cd
umount /mnt

mount /dev/nvme0n1p2 -o compress=zstd:2,subvol=debian/@root /mnt
mkdir -p /mnt/{home,.snapshots,boot/efi}
mount /dev/nvme0n1p2 -o compress=zstd:2,subvol=debian/@home /mnt/home
mount /dev/nvme0n1p2 -o compress=zstd:10,subvol=debian/@snapshots /mnt/.snapshots


mount /dev/nvme0n1p1 /mnt/boot/efi
```
### install system:
`debootstrap --arch amd64 stable /mnt https://deb.debian.org/debian`

```
mount -t proc /proc /mnt/proc/
mount -t sysfs /sys /mnt/sys/
mount --rbind /dev /mnt/dev/
mount --rbind /run /mnt/run/

chroot /mnt /bin/bash

```
add the following to the single line in `/etc/apt/sources.list`:
`contrib non-free non-free-firmware`
for myself: i have added the following into my apt sources, and added it to the list of installed packages:
```
echo 'deb [trusted=yes] https://repo.vivaldi.com/archive/deb/ stable main' | tee /etc/apt/sources.list.d/vivaldi.list
```

install packages: note the last 3 after vivaldi are for ly only
```
apt update
apt install linux-image-amd64 vim amd64-microcode locales firmware-linux chrony make gcc git libx11-dev libxft-dev libxinerama-dev xorg sudo btrfs-progs gdisk usbutils unzip feh picom pipewire pavucontrol vivaldi-stable wget build-essential libpam0g-dev libxcb-xkb-dev

/usr/sbin/dpkg-reconfigure tzdata
/usr/sbin/dpkg-reconfigure locales
/usr/sbin/locale-gen
rm /etc/hostname && echo "debianPC" >> /etc/hostname
```
make sure the following exists in `/etc/hosts`:
```
127.0.0.1       localhost
127.0.1.1       debianPC
```
exit chroot then run 
```

genfstab -U /mnt >> /mnt/etc/fstab
```
edit fstab to look like the following: PLEASE REMEMVER TO FIX !!!!!
```
a
```

return into chroot, git clone my rEFInd repo [here](https://gitlab.com/JohnCorbs/refind) and setup accordingly, details in repo

```
useradd -D -s /bin/bash
passwd
useradd -m user
passwd user
```
might want to install ly & dwm/st/dmenu first
```
apt install systemd-resolved
systemctl enable systemd-networkd systemd-resolved
```
`vim /etc/systemd/network/20-wired.network`
```
[Match]
Name=enp5s0
[Network]
Address=10.0.0.100/24
Gateway=10.0.0.1
DNS=10.0.0.1
IgnoreCarrierLoss=3s

```

`systemctl enable chrony`


`efibootmgr -c -d /dev/nvme0n1p1 -l \\refind\\refind_x64.efi -L "rEFInd"`

## Audio

With pipewire & pavu-control already being installed,, audio workes it seems. 

### Suspend & Hibernate
