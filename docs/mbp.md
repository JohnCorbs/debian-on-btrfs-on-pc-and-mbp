[home](https://gitlab.com/JohnCorbs/debian-on-btrfs-on-pc-and-mbp)
# Linux on MacbookPro13,3
My own guide in attempting to get linux running well on my MacBooks
Any comments, suggestions, etc. are greatly apreciated.

[TOC]

## prerequisits:

**Have a existing instance of linux with btrfs-progs, debootstrap, and an internet connection**
a link to my guide on creating such usb can be found [here](https://gitlab.com/-/snippets/3603802)

I will show you here how to get wifi running on the usb in question:
download the firmware file that can be found [here](https://bugzilla.kernel.org/show_bug.cgi?id=193121#c74)
mount root partition of usb.
mv ~/Downloads/brcmfmac43602-pcie.txt /mnt/lib/firmware/brcm
boot usb on macbook
`modprobe brcmfmac`

## Install

### make sure wifi is connected:
[here](https://wiki.archlinux.org/title/MacBookPro11,x#Internet)

### partitions ur disk & mount:
```
≥512M EFI partition:
    ef00 type
    mkfs.fat -F 32 /dev/nvme0n1p1
Partition for macos:
    8300 type
   mkfs.fat -F 32 /dev/nvme0n1p2
Partition for btrfs:
    8300 type
    mkfs.btrfs /dev/nvme0n1p3
Partition for swap:
    8200 type
    in gdisk starting sector is -16G
```
I prefer to install macos first.
since im starting this all fresh, i partitioned in linux first,
then booted macos installer to partition /dev/sdx2, and install onto there.
```
mount /dev/nvme0n1p3 /mnt
cd /mnt
mkdir debian
btrfs subvolume create debian/@root
btrfs subvolume create debian/@home

btrfs subvolume create debian/@snapshots

cd
umount /mnt

mount /dev/nvme0n1p3 -o compress=zstd:2,subvol=debian/@root /mnt
mkdir -p /mnt/{home,.snapshots,boot/efi}
mount /dev/nvme0n1p3 -o compress=zstd:2,subvol=debian/@home /mnt/home
mount /dev/nvme0n1p3 -o compress=zstd:10,subvol=debian/@snapshots /mnt/.snapshots


mount /dev/nvme0n1p1 /mnt/boot/efi
```
### install system:
`debootstrap --arch amd64 stable /mnt https://deb.debian.org/debian`

```
mount -t proc /proc /mnt/proc/
mount -t sysfs /sys /mnt/sys/
mount --rbind /dev /mnt/dev/
mount --rbind /run /mnt/run/

chroot /mnt /bin/bash

```
add the following to the single line in `/etc/apt/sources.list`:
`contrib non-free non-free-firmware`
for myself: i have added the following into my apt sources, and added it to the list of installed packages:
```
echo 'deb [trusted=yes] https://repo.vivaldi.com/archive/deb/ stable main' | tee /etc/apt/sources.list.d/vivaldi.list
```

install packages: note the last 3 after vivaldi are for ly only
```
apt update
apt install linux-image-amd64 vim intel-microcode locales firmware-linux chrony make gcc git libx11-dev libxft-dev libxinerama-dev xorg sudo network-manager wireless-tools btrfs-progs firmware-b43-installer gdisk usbutils unzip firmware-brcm80211 feh picom pipewire pavucontrol vivaldi-stable wget build-essential libpam0g-dev libxcb-xkb-dev

/usr/sbin/dpkg-reconfigure tzdata
/usr/sbin/dpkg-reconfigure locales
/usr/sbin/locale-gen
rm /etc/hostname && echo "debianMacBook" >> /etc/hostname
```
make sure the following exists in `/etc/hosts`:
```
127.0.0.1       localhost
127.0.1.1       debianMacBook
```
exit chroot then run 
```

genfstab -U /mnt >> /mnt/etc/fstab
```
edit fstab to look like the following: REMEMBER TO FIX !!!
```
a
```

return into chroot, git clone my rEFInd repo [here](https://gitlab.com/JohnCorbs/refind) and setup accordingly, details in repo

```
useradd -D -s /bin/bash
passwd
useradd -m user
passwd user
```
`systemctl enable NetworkManager chrony`

`efibootmgr -c -d /dev/sdx1 -l \\refind\\refind_x64.efi -L "rEFInd"`

## Post Install:

### Wifi

```
iwconfig wlp3s0 txpower 10dBm
```
this command needs to be run manually after boot,saw a solution at some point but cant find it now.
2.4 only should work.
grap this file from [here](https://bugzilla.kernel.org/show_bug.cgi?id=193121#c72) (comment 72), throw it in `/lib/firmware/brcm/`


### Audio
From [this repo](https://github.com/davidjo/snd_hda_macbookpro) do the following (copy paste):
```
cd .config
git clone https://github.com/davidjo/snd_hda_macbookpro.git
cd snd_hda_macbookpro/

sudo ./install.cirrus.driver.sh
reboot
```
With pipewire & pavu-control already being installed,, audio workes it seems. 
### WebCam
works(in vivaldi at least)


### Intel GPU
in `redind.conf` add the following line:
```
spoof_osx_version 10.15
```


### Bluetooth


### Suspend & Hibernate
