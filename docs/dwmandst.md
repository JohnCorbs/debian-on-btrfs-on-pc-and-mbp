[home](https://gitlab.com/JohnCorbs/debian-on-btrfs-on-pc-and-mbp)
# dwm and st
this file will cover the installation and configuration of dwm and st onto my pc's

[TOC]

## to install
```
* as user *
mkdir /home/user/.config
cd /home/user/.config
git clone https://gitlab.com/JohnCorbs/dwm
git clone https://gitlab.com/JohnCorbs/st
git clone https://gitlab.com/JohnCorbs/dmenu
sudo make clean install
```
## add as entry for ly:
`vim /usr/share/xsessions/dwm.desktop`
```
[Desktop Entry]
Name=dwm
Exec=/usr/local/bin/dwm
TryExec=/usr/local/bin/dwm
Type=Application
```
## git:
### updating:
### applying patches:

## Media buttons:
See link on home page, its not that hard.

## to do list:
