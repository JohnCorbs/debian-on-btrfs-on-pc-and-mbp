[home](https://gitlab.com/JohnCorbs/debian-on-btrfs-on-pc-and-mbp)
## Wallpapers:

With `feh` being installed, and the entry already in the autostart section of the dwm conf, wallpapers should work. wallpaper dir. is already set in autostart command to `home/user/Pictures/Wallpapers`

## Transparency:

Again with `picom` being installed and in the autostart and in dwm it should work. thumbs up
