## Debian on btrfs on PC and MBP
Consolidation of my 2 seperate repos for my PC and my MacBook Pro 13,3. this repo is intended to be for my own personal reference. This repo is meant to do the following:
1. List how I might install debian on btrfs on my pc and macbook, including things specivic to either or
2. List how I might setup this system to my liking with my software

should be noted: im doing a swap partition because I couldnt figure out the swapfile in a subvolume. plan on fixing later.

## Directory

### Install
- [PC](https://gitlab.com/JohnCorbs/debian-on-btrfs-on-pc-and-mbp/-/blob/main/docs/pc.md)
- [macbookpro 13,3](https://gitlab.com/JohnCorbs/debian-on-btrfs-on-pc-and-mbp/-/blob/main/docs/mbp.md)

### Post-install
- [other](https://gitlab.com/JohnCorbs/debian-on-btrfs-on-pc-and-mbp/-/blob/main/docs/other.md)
- [dwm & st](https://gitlab.com/JohnCorbs/debian-on-btrfs-on-pc-and-mbp/-/blob/main/docs/dwmandst.md)
- [ly](https://gitlab.com/JohnCorbs/debian-on-btrfs-on-pc-and-mbp/-/blob/main/docs/ly.md)
- [wallpaper & transparency](https://gitlab.com/JohnCorbs/debian-on-btrfs-on-pc-and-mbp/-/blob/main/docs/wallpaper&transparency.md)
- [raid(pc only)](https://gitlab.com/JohnCorbs/debian-on-btrfs-on-pc-and-mbp/-/blob/main/docs/raid.md)


## Central list of shit to do
### in general
- [ ] how to do the $PATH thing right
- [ ] good terminal file explorer with photo preview, open and videos too
### pc
- [ ] suspend and hibernate stuff
- [x] the raid array
### mbp
- [x] 'sudo: unable to resolve host debianMacBook: Temporary failure in name resolution'
- [ ] the weird xorg thing at ly if it persists, with bootup thing
- [ ] audio and video manager
- [x] 5Ghz
- [ ] suspend & hybernate
- [ ] power management
- [x] gpu switching
- [ ] bluetooth
- [ ] mouse sensitivity/scroll sensitivity/2 finger scroll in vivaldi/tap to click
### dwm/st/dmenu/overall desktop experience
- [ ] scaling of it all
- [ ] git stuff, how to easy setup, ssh keys, etc
- [ ] divy up the repos; either seperate repositories for pc and mbp, or seperate files
- [x] write page on the doing wallpaper and transparency
- [ ] finish writing bar script, mainly for mbp (cat /sys/class/power_supply/BAT0/capacity)

## Links
### Debian/btrfs/pc generic
- https://www.linuxquestions.org/questions/debian-26/how-to-install-debian-using-debootstrap-4175465295/
- https://gist.github.com/varqox/42e213b6b2dde2b636ef#mount-filesystem
- https://www.debian.org/releases/buster/amd64/apds03.en.html
- https://gist.github.com/varqox/42e213b6b2dde2b636ef
- https://www.debian.org/releases/buster/amd64/apds03.en.html
- https://serverfault.com/questions/976289/bash-dpkg-reconfigure-command-not-found
- https://wiki.debian.org/DateTime

### mbp specific
- https://github.com/Dunedan/mbp-2016-linux#wi-fi 
- https://gist.github.com/cristianmiranda/6f269797b62076c3414c3baa848dda67 
- https://bugzilla.kernel.org/show_bug.cgi?id=193121
- https://wiki.debian.org/MacBook/Wireless
- https://wiki.archlinux.org/title/Mac
- https://askubuntu.com/a/60395
[intel graphics](https://github.com/Dunedan/mbp-2016-linux/issues/6#issuecomment-286168538)
[audio](https://github.com/davidjo/snd_hda_macbookpro)
[keyboard volume](see [here](https://wiki.archlinux.org/title/WirePlumber#Keyboard_volume_control) for volume control)


### other
- https://forum.vivaldi.net/post/518014

### dwm/st specific
- https://gitlab.com/JohnCorbs/dwm
- https://gitlab.com/JohnCorbs/st
- https://suckless.org
- https://bbs.archlinux.org/viewtopic.php?id=58298

### ly specific
- https://wiki.archlinux.org/title/display_manager
- https://github.com/fairyglade/ly

### wallpapers
- 


[template](https://gitlab.com/JohnCorbs/debian-on-btrfs-on-pc-and-mbp/-/blob/main/docs/template.md)